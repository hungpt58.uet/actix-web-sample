use actix_web::{App, dev, error, guard, Handler, HttpResponse, HttpServer, web};
use actix_web::http::{header, StatusCode};
use actix_web::middleware::{DefaultHeaders, ErrorHandlerResponse, ErrorHandlers, Logger};
use env_logger::Env;

use crate::api::guard::ContentTypeHeader;

mod api;
mod domain;
mod util;

fn add_error_header<B>(mut res: dev::ServiceResponse<B>) -> actix_web::Result<ErrorHandlerResponse<B>> {
    res.response_mut().headers_mut().insert(
        header::CONTENT_TYPE,
        header::HeaderValue::from_static("Error"),
    );

    Ok(ErrorHandlerResponse::Response(res.map_into_left_body()))
}

mod app_actor {
    use actix::prelude::*;

    #[derive(Message)]
    #[rtype(result = "usize")]
    struct Ping(usize);

    struct MyActor {
        count: usize
    }

    impl Actor for MyActor {
        type Context = Context<Self>;

        fn started(&mut self, ctx: &mut Context<Self>) {
            println!("Actor is alive");
        }

        fn stopped(&mut self, ctx: &mut Context<Self>) {
            println!("Actor is stopped");
        }
    }

    impl Handler<Ping> for MyActor {
        type Result = usize;

        fn handle(&mut self, msg: Ping, _ctx: &mut Context<Self>) -> Self::Result {
            self.count += msg.0;
            self.count
        }
    }

    pub async fn actor_main() {
        let addr = MyActor { count: 10 }.start();

        // send message and get future for result
        let res = addr.send(Ping(10)).await;

        // handle() returns tokio handle
        println!("RESULT: {}", res.unwrap() == 20);
    }
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    std::env::set_var("RUST_LOG", "info");
    std::env::set_var("RUST_BACKTRACE", "1");
    env_logger::init_from_env(Env::default().default_filter_or("info"));

    app_actor::actor_main().await;

    HttpServer::new(|| {
        let json_config = web::JsonConfig::default()
            .limit(4096)
            .error_handler(|err, _req| {
                error::InternalError::from_response(err, HttpResponse::Conflict().finish())
                    .into()
            });

        let state = web::Data::new(
            domain::AppState::new("Actix-Web-Sample")
        );

        let with_host_guard = web::scope("/guard")
            .guard(guard::Header("Host", "www.rust-lang.org"))
            .service(api::greeting::hello);

        App::new()
            .app_data(state)
            .app_data(json_config)

            .service(api::greeting::hello)
            .service(api::greeting::check_counter)
            .service(api::greeting::get_counter_n)
            .service(api::greeting::read_info)
            .service(api::greeting::get_error)

            .service(with_host_guard)
            .default_service(
                web::route()
                    .guard(guard::Not(guard::Get()))
                    .to(HttpResponse::MethodNotAllowed),
            )
            .wrap(Logger::default())
            .wrap(DefaultHeaders::new().add(("X-Version", "1.0")))
            .wrap(
                ErrorHandlers::new()
                    .handler(StatusCode::BAD_REQUEST, add_error_header),
            )
    })
        .bind(("127.0.0.1", 8080))?
        .run()
        .await
}
