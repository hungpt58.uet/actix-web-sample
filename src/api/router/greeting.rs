use actix_web::{get, post, Responder, web::*};

use crate::api::ApiRes;
use crate::api::dto::{Info, ReqParam};
use crate::domain::AppState;
use crate::util::error::AppError;

#[get("/")]
pub async fn hello(data: Data<AppState>) -> impl Responder {
    let mut counter = data.counter.lock().unwrap();
    *counter += 1;

    format!("Welcome to {} in {} times", &data.app_name, *counter)
}

#[get("/counters/{n}")]
pub async fn check_counter(path: Path<i32>, data: Data<AppState>) -> impl Responder {
    let count_n = path.into_inner();
    let cur_counter = &data.counter.lock().unwrap().abs();

    if *cur_counter >= count_n {
        format!("In range !!")
    } else {
        format!("Out range !!")
    }
}

#[get("/counters")]
pub async fn get_counter_n(q: Query<ReqParam>, data: Data<AppState>) -> impl Responder {
    let cur_counter = &data.counter.lock().unwrap();
    let idx = q.0.q;
    let flag = q.0.n.unwrap_or_default();

    format!("Idx: {} -> Flag: {} : In range >> {}", idx, flag, *cur_counter)
}

#[post("/info")]
pub async fn read_info(info: Json<Info>) -> ApiRes<Info> {
    if info.tag > 4 {
        Ok(info.0)
    } else {
        Err(AppError::BadClientData)
    }
}

#[get("/errors")]
pub async fn get_error() -> Result<&'static str, AppError> {
    Err(AppError::BadClientData)
}