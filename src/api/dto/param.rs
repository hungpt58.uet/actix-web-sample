use serde::Deserialize;

#[derive(Deserialize)]
pub struct ReqParam {
    pub q: i32,
    pub n: Option<i32>
}