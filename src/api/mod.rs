pub use router::greeting;

use crate::util::error::AppError;

mod router;
mod common;
mod dto;
pub mod guard;

pub type ApiRes<T> = Result<T, AppError>;
