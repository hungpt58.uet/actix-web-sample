use actix_web::guard::{Guard, GuardContext};
use actix_web::http;

pub struct ContentTypeHeader;

impl Guard for ContentTypeHeader {
    fn check(&self, req: &GuardContext) -> bool {
        println!("sdsds");
        req.head()
            .headers()
            .contains_key(http::header::CONTENT_TYPE)
    }
}