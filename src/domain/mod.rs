use std::sync::Mutex;

pub mod user;

pub struct AppState {
    pub app_name: String,
    pub counter: Mutex<i32>,
}

impl AppState {
    pub fn new(app_name: &str) -> Self {
        Self {
            app_name: app_name.to_string(),
            counter: Mutex::new(0),
        }
    }
}